#ifndef APP_H
#define APP_H

#include <memory>

#ifdef _WIN32
	#include <Windows.h>
#endif

#include <GL/gl.h>
#include <GL/glu.h>
#include <SDL2/SDL.h>
#include "exceptions.h"

#include "model3d.h"

class App
{
private:
    SDL_Window* _window;
    SDL_Renderer* _renderer;
    SDL_GLContext _glContext;

    bool _running;
    std::unique_ptr<Model3D> _model;

    void setup();
    void initGL();
    void input();
    void update();
    void render();
    void cleanup();

public:
    App();
    ~App();

    int run();
};

#endif // APP_H
