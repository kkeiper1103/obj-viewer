#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <string>
#include <exception>

class AppException : public std::exception {
private:
    std::string _mesg;

public:
    AppException(std::string mesg) : _mesg(mesg)
    {}

    ~AppException() throw() {}

    virtual const char* what() const throw() {
        return _mesg.c_str();
    }
};

#endif // EXCEPTIONS_H
