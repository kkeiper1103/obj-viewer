TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

LIBS += -lSDL2 -lGL -lGLU

SOURCES += main.cpp \
    app.cpp \
    model3d.cpp \
    tiny_obj_loader.cpp

HEADERS += \
    app.h \
    exceptions.h \
    model3d.h \
    tiny_obj_loader.h

