#ifndef MODEL3D_H
#define MODEL3D_H

#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>

#ifdef _WIN32
	#include <Windows.h>
#endif

#include <SDL2/SDL.h>
#include <GL/gl.h>

#include <GL/glext.h>

#ifdef __linux__
#include <GL/glx.h>
#endif

#include <GL/glu.h>

#include <glm/glm.hpp>

#include "exceptions.h"


#include "tiny_obj_loader.h"


class Model3D
{
private:
    std::string _filename;

    std::vector<tinyobj::shape_t> _shapes;
    std::vector<tinyobj::material_t> _materials;

    std::map<SDL_Scancode, bool> _keys = {
        {SDL_SCANCODE_LEFT, false},
        {SDL_SCANCODE_RIGHT, false},
        {SDL_SCANCODE_UP, false},
        {SDL_SCANCODE_DOWN, false},
        {SDL_SCANCODE_R, false}
    };


    bool _rotate = true;
    float _horizontalAngle = 0.0f;
    float _verticalAngle = 0.0f;
    float _distance = -512.0f;

public:
    Model3D(std::string filename, std::string working_directory = "");
    ~Model3D();

    void input(SDL_Event& event);
    void update();
    void renderTo(SDL_Renderer* renderer);
};

#endif // MODEL3D_H
