#include "app.h"

/**
 * @brief App::setup
 */
void App::setup()
{
    if( SDL_Init(SDL_INIT_EVERYTHING) < 0 ) {
        throw AppException( SDL_GetError() );
    }

    /**
     *
     */
    _window = SDL_CreateWindow("Obj Model Viewer", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                               640, 480, SDL_WINDOW_OPENGL );

    if( _window == nullptr ) {
        throw AppException( SDL_GetError() );
    }

    /**
     *
     */
    _renderer = SDL_CreateRenderer( _window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC );

    if( _renderer == nullptr ) {
        throw AppException( SDL_GetError() );
    }


    // gl 2.1
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 2 );
    SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 1 );


    //
    _glContext = SDL_GL_CreateContext(_window);
    if( _glContext == nullptr ) {
        throw AppException( SDL_GetError() );
    }


    /*if( SDL_GL_SetSwapInterval(1) < 0 ) {
        throw AppException( SDL_GetError() );
    }*/

    initGL();


    //
    _model = std::unique_ptr<Model3D>(new Model3D("Building_dnt_V02.obj"));

    _running = true;
}

/**
 * @brief App::initGL
 */
void App::initGL()
{
    GLenum error = GL_NO_ERROR;

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // @todo error checking


    gluPerspective(45.0f, 640.0f / 480.0f, 0.0f, 1024.0f);
    // glFrustum(0.0f, 640.0f, 480.0f, 0.0f, 0.0f, 512.0f);


    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    // @todo error checking


    // enable texturing
    glEnable( GL_TEXTURE_2D );


    // set blending options
    glEnable( GL_BLEND );
    glDisable( GL_DEPTH_TEST );
    glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );


    // set the rendercolor to black
    glClearColor(0.f, 0.f, 0.f, 1.f);
}

/**
 * @brief App::input
 */
void App::input()
{
    SDL_Event event;
    while(SDL_PollEvent(&event)) {

        if(event.type == SDL_QUIT) {
            _running = false;
        }

        else {
            _model->input(event);
        }
    }
}

/**
 * @brief App::update
 */
void App::update()
{
    _model->update();
}

/**
 * @brief App::render
 */
void App::render()
{

    //


    // maybe unnecessary param?
    _model->renderTo( _renderer );

    SDL_GL_SwapWindow(_window);
}

/**
 * @brief App::cleanup
 */
void App::cleanup()
{
    SDL_DestroyRenderer(_renderer);
    SDL_DestroyWindow(_window);

    SDL_Quit();
}

/**
 * @brief App::App
 */
App::App()
{

}

/**
 * @brief App::~App
 */
App::~App()
{

}

/**
 * @brief App::run
 * @return
 */
int App::run()
{
    setup();

    while(_running) {
        input();
        update();

        render();
    }

    cleanup();

    return 0;
}
