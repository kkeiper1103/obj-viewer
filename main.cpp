#include <iostream>

#include "app.h"

#include "exceptions.h"

#ifdef _MSC_VER
#undef main
#endif

int main(int argc, char* argv[])
{
    try {
        App app;

        return app.run();
    }
    catch(AppException& e) {
        std::cerr << "Exception Occurred: " << e.what() << std::endl;
        return -1;
    }
}