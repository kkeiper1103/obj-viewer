#include "model3d.h"

Model3D::Model3D(std::string filename, std::string working_directory) : _filename(filename)
{

    std::string err;
    bool ret = tinyobj::LoadObj(_shapes, _materials, err, filename.c_str(), working_directory.c_str());

    if(!err.empty()) {
        std::cout << err << std::endl;
    }

    std::cout << "# of shapes: " << _shapes.size() << std::endl;
    std::cout << "# of materials: " << _materials.size() << std::endl;


    /*

    for (size_t i = 0; i < _materials.size(); i++) {
      printf("material[%ld].name = %s\n", i, _materials[i].name.c_str());
      printf("  material.Ka = (%f, %f ,%f)\n", _materials[i].ambient[0], _materials[i].ambient[1], _materials[i].ambient[2]);
      printf("  material.Kd = (%f, %f ,%f)\n", _materials[i].diffuse[0], _materials[i].diffuse[1], _materials[i].diffuse[2]);
      printf("  material.Ks = (%f, %f ,%f)\n", _materials[i].specular[0], _materials[i].specular[1], _materials[i].specular[2]);
      printf("  material.Tr = (%f, %f ,%f)\n", _materials[i].transmittance[0], _materials[i].transmittance[1], _materials[i].transmittance[2]);
      printf("  material.Ke = (%f, %f ,%f)\n", _materials[i].emission[0], _materials[i].emission[1], _materials[i].emission[2]);
      printf("  material.Ns = %f\n", _materials[i].shininess);
      printf("  material.Ni = %f\n", _materials[i].ior);
      printf("  material.dissolve = %f\n", _materials[i].dissolve);
      printf("  material.illum = %d\n", _materials[i].illum);
      printf("  material.map_Ka = %s\n", _materials[i].ambient_texname.c_str());
      printf("  material.map_Kd = %s\n", _materials[i].diffuse_texname.c_str());
      printf("  material.map_Ks = %s\n", _materials[i].specular_texname.c_str());
      printf("  material.map_Ns = %s\n", _materials[i].specular_highlight_texname.c_str());
      std::map<std::string, std::string>::const_iterator it(_materials[i].unknown_parameter.begin());
      std::map<std::string, std::string>::const_iterator itEnd(_materials[i].unknown_parameter.end());
      for (; it != itEnd; it++) {
        printf("  material.%s = %s\n", it->first.c_str(), it->second.c_str());
      }
      printf("\n");
    }
*/

}

Model3D::~Model3D()
{

}

/**
 * @brief Model3D::input
 * @param event
 */
void Model3D::input(SDL_Event& event)
{
    switch(event.type) {

    case SDL_KEYDOWN:
        _keys[event.key.keysym.scancode] = true;
        break;

    case SDL_KEYUP:
        _keys[event.key.keysym.scancode] = false;
        break;

    // if both moving and clicked
    case SDL_MOUSEMOTION:

        /**
        if( event.button.button == SDL_BUTTON(SDL_BUTTON_LEFT) ) {

            _horizontalAngle -= (event.motion.x / 4);
            _verticalAngle -= (event.motion.y / 4);
        }
        */

        break;

    case SDL_MOUSEWHEEL:

        _distance -= (10 * event.wheel.y);

        break;

    default:
        break;
    }
}

/**
 * @brief Model3D::update
 */
void Model3D::update()
{
    if( _keys[SDL_SCANCODE_LEFT] ) {
        _horizontalAngle--;

        if( _horizontalAngle >= 360.0f ) {
            _horizontalAngle = 0.0f;
        }
        else if( _horizontalAngle < 0 ) {
            _horizontalAngle = 359.0f;
        }
    }
    else if( _keys[SDL_SCANCODE_RIGHT] ) {
        _horizontalAngle++;

        if( _horizontalAngle >= 360.0f ) {
            _horizontalAngle = 0.0f;
        }
        else if( _horizontalAngle < 0 ) {
            _horizontalAngle = 359.0f;
        }
    }


    if( _keys[SDL_SCANCODE_UP] ) {
        _verticalAngle++;

        if( _verticalAngle >= 360.0f ) {
            _verticalAngle = 0.0f;
        }
        else if( _verticalAngle < 0 ) {
            _verticalAngle = 359.0f;
        }
    }
    else if( _keys[SDL_SCANCODE_DOWN] ) {
        _verticalAngle--;

        if( _verticalAngle >= 360.0f ) {
            _verticalAngle = 0.0f;
        }
        else if( _verticalAngle < 0 ) {
            _verticalAngle = 359.0f;
        }
    }
}

/**
 * @brief Model3D::renderTo
 * @param renderer
 */
void Model3D::renderTo(SDL_Renderer *renderer)
{
    glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glLoadIdentity();

    //
    glColor3f( 1.0f, 1.0f, 1.0f );


    glTranslatef( 0.0f, 0.0f, _distance );

    // to flip horizontally, the pivot axis needs to be the Y axis
    glRotatef( _horizontalAngle, 0.0f, 1.0f, 0.0f );

    // vice versa
    glRotatef( _verticalAngle, 1.0f, 0.0f, 0.0f );




    glBegin( GL_TRIANGLES );
    for( size_t i=0; i < _shapes.size(); i++ ) {

        for (size_t v = 0; v < _shapes[i].mesh.indices.size() / 3; v++) {
            //
            /*glVertex3f(
                _shapes[i].mesh.indices[3*v+0],
                _shapes[i].mesh.indices[3*v+1],
                _shapes[i].mesh.indices[3*v+2]
            );*/
        }


        for (size_t v = 0; v < _shapes[i].mesh.positions.size() / 3; v++) {


            glVertex3f(
                _shapes[i].mesh.positions[3*v+0],
                _shapes[i].mesh.positions[3*v+1],
                _shapes[i].mesh.positions[3*v+2]
            );
        }
    }
    glEnd();

}
